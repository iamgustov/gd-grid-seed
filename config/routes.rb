Rails.application.routes.draw do
  root 'tests#jq_grid'

  scope '/city' do
    get '/list(.:format)', to: 'cities#index', as: 'cities_index'
    get '/sample(.:format)', to: 'cities#sample', as: 'cities_sample'
    get '/jq-batch', to: 'cities#jq_batch', as: 'cities_jq_batch'
    get '/ng-batch', to: 'cities#ng_batch', as: 'cities_ng_batch'
  end

  scope '/test' do
    get '/jq-grid', to: 'tests#jq_grid', as: 'tests_jq_grid'
    get '/ng-grid', to: 'tests#ng_grid', as: 'tests_ng_grid'
    get '/jq-widgets', to: 'tests#jq_widgets', as: 'tests_jq_widgets'
    get '/kendo', to: 'tests#kendo_grid', as: 'tests_kendo_grid'
    get '/holdings-print', to: 'fakes#holdings_print', as: 'fakes_holdings_print'
    get '/holdings-print-schema', to: 'fakes#holdings_print_schema', as: 'fakes_holdings_print_schema'
  end
end
