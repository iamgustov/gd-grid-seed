class City < ActiveRecord::Base
  class << self
    def random
      limit(1).offset(rand(count)).first
    end
  end

  def in_json
    {
        id: self.id,
        name: self.name,
        zip: self.zip,
        time_zone: self.time_zone,
        state: self.state,
        country: self.country
    }
  end
end
