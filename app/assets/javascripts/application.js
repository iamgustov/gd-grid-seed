// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
// require jquery
// require jquery_ujs
// require turbolinks
// require_self
// require_tree .

var Faker = (function () {
    var instance = {};

    instance.generateAccount = function () {
        return {
            acctName: 'James, TA Trust Capital Access-' + (Math.floor(Math.random() * (100 - 10 + 1)) + 10),
            acctNum: '50450043',
            amountInv: null,
            avgCostShare: null,
            avgInvShare: null,
            costBasis: null,
            costBasisGL: null,
            costBasisGLPerc: null,
            current: 29426243.59,
            cusip: null,
            daily: null,
            dailyPerc: null,
            desc: '2514 DIVERSIFIED L.P.',
            disp: '',
            estAnnInc: null,
            estAnnYield: null,
            hasOpen: false,
            hasTaxlots: false,
            id: '50450043_Cash',
            amountInvGL: null,
            amountInvGLPerc: null,
            isMarginTypeLoc: false,
            isTrust: false,
            mAgreements: false,
            percPort: 0.0398826162095903,
            plans: null,
            pType: null,
            sourceAcct: null,
            price: null,
            priceChg: null,
            priceChgPerc: null,
            priceDate: null,
            prdType: 0,
            detailedPrdType: 0,
            quan: null,
            rschRating: null,
            shortName: 'JAMES,TA CAP AC',
            suitability: 0,
            symCusip: 'CASH',
            typeLocs: null,
            isSweepOption: true,
            underlying: null,
            isMoney: true,
            dSym: 'CASH',
            coupon: null,
            debt: ' ',
            factor: null,
            fCallDate: null,
            fCallPrice: null,
            hasPrerefund: false,
            maturity: null,
            modDuration: null,
            nextPayAmount: null,
            nextPayDate: null,
            par: null,
            ratings: null,
            remainPrincVal: null,
            sCallDate: null,
            sCallPrice: null,
            cashBalances: [
                {
                    sortOrder: 3,
                    description: 'EAGLE CLASS - JPMORGAN TAX FREE MONEY MARKET FUND*',
                    amount: 29426243.59,
                    estAnnInc: 2942.62,
                    estAnnYield: 0.009999985186692326
                }
            ],
            yestValue: null,
            isMCB: false,
            isMAI: false,
            isCash: false,
            currency: ' ',
            exRate: null,
            nonUsPrice: null,
            nonUsValue: null,
            cultureCode: null,
            advisor: '0002',
            status: 0,
            ar: 'tsg',
            ac: 'tst',
            stc: null,
            mm5: null,
            pc: null,
            pi: 'cash',
            psc: 'brk',
            marginable: false,
            policy: null,
            grpSrt: null,
            multiTradable: false,
            tradeMenuType: 7,
            selected: false
        }
    };

    instance.generateAccounts = function (times, callback) {
        var data = [];

        for (var i = times; i > 0; i--) {
            data.push(this.generateAccount());
        }

        return callback(data, Array.prototype.slice.call(arguments, 2));
    };

    return instance;
})();