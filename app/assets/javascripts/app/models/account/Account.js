'use strict';

(function (angular, module) {
    module.factory('Account', function ($q, $http) {
        var instance = {},
            api = {
                batch: '/test/holdings-print.json'
            };

        instance.loadBatch = function () {
            return this.sendResponse(api.batch);
        };

        instance.generateItems = function (callback) {
            if (callback != null) {
                return Faker.generateAccounts(20000, callback, Array.prototype.slice.call(arguments, 1));
            }

            return Faker.generateAccounts(20000, function (data) {
                return data
            });
        };

        instance.sendResponse = function (apiPath) {
            var deffered = $q.defer();

            $http.get(apiPath).success(function (data) {
                deffered.resolve(data);
            }).error(function (data) {
                deffered.resolve({});
            });

            return deffered.promise;
        };

        return instance;
    });
})(angular, modelsModule);