'use strict';

(function (angular, module) {
    module.factory('Place', function($q, $http) {
        var place = {},
            api = {
                batch: '/city/ng-batch.json'
            };

        place.loadBatch = function() {
            return this.sendResponse(api.batch);
        };

        place.sendResponse = function(apiPath) {
            var deffered = $q.defer();

            $http.get(apiPath).success(function(data) {
                deffered.resolve(data);
            }).error(function(data) {
                deffered.resolve({});
            });

            return deffered.promise;
        };

        return place;
    });
})(angular, modelsModule);