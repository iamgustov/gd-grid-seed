'use strict';

(function (angular, module) {
    module.controller('NGGridController', ['$scope', '$http', 'Account', function ($scope, $http, Account) {
        $scope.data = {
            //grid: Account.loadBatch(),
            grid: Account.generateItems(function(data) {return data;}),
            gridOptions: {}
        };
        $scope.config = {
            dataLoaded: false
        };

        $scope.$watch('data.grid', function () {
            //$scope.data.grid = result;
            $scope.config.dataLoaded = !$scope.config.dataLoaded;
        });

        //$scope.data.grid.then(function (result) {
        //    $scope.data.grid = result;
        //    $scope.config.dataLoaded = true;
        //});

        $scope.data.gridOptions = {
            data: 'data.grid',
            enablePinning: true,
            virtualizationThreshold: 2,
            showFooter: true,
            showGroupPanel: true,
            enableColumnResize: true,
            showMenu: true,
            columnDefs: [
                {displayName: 'Account Name', field: 'acctName'},
                {displayName: 'Account No.', field: 'acctNum'},
                {displayName: 'Description', field: 'desc'},
                {displayName: 'Symbol/CUSIP', field: 'dSym'},
                {displayName: 'Quantity', field: 'current'},
                {displayName: 'Price', field: 'costBasisGLPerc'},
                {displayName: 'Avg. Invest. Per Share', field: 'hasOpen'},
                {displayName: 'Current Value', field: 'plans'},
                {displayName: 'Amount Invested', field: 'prdType'},
                {displayName: 'Invest. GL', field: 'quan'},
                {displayName: 'Est. Annual Income', field: 'shortName'}
            ]
        };
    }]);
})(angular, controllersModule);