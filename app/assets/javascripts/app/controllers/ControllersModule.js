'use strict';

(function(angular, global) {
    var controllersModule = angular.module('gridApp.controllers', []);
    global.controllersModule = controllersModule;
})(angular, window);