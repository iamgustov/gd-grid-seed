'use strict';

(function (angular, module) {
    module.controller('JQWidgetController', ['$scope', 'Place', 'Account', function ($scope, Place, Account) {
        $scope.data = {
            //accounts: Account.loadBatch(),
            accounts: Account.generateItems(function (data) {
                return data;
            }),
            loaded: false
            //places: Place.loadBatch()
        };

        $scope.$watch('data.accounts', function () {
            $scope.data.loaded = true;
            $scope.config.settings.source = $scope.data.accounts;
        });

        //$scope.data.accounts.then(function(data) {
        //    $scope.data.accounts = data;
        //    $scope.data.loaded = true;
        //    $scope.config.settings.source = $scope.data.accounts;
        //});

        //$scope.data.places.then(function (data) {
        //    data[0]['name'] = '<span class="-error">{name}</span>'.replace('{name}', data[0]['name']);
        //    $scope.data.places = data;
        //    $scope.config.settings.source = $scope.data.places;
        //});

        $scope.config = {
            columnState: {}
        };

        $scope.renderCell = function (row, columnfield, value, defaulthtml, columnproperties) {
            if (row % 2 == 0) {
                return '<div class="-success" style="margin: 4px 2px 2px 4px;">{value}</div>'.replace('{value}', value);
            }
        };

        $scope.config.settings = {
            altrows: true,
            width: '100%',
            height: 500,
            sortable: true,
            groupable: true,
            theme: 'energyblue',
            columnsresize: true,
            //virtualmode: true,
            //rendergridrows: function(obj)
            //{
            //    return obj.data;
            //},
            source: [],
            columns: [
                {text: 'Account Name', dataField: 'acctName', cellsrenderer: $scope.renderCell},
                {text: 'Account No.', dataField: 'acctNum'},
                {text: 'Description', dataField: 'desc'},
                {text: 'Symbol/CUSIP', dataField: 'dSym'},
                {text: 'Quantity', dataField: 'current'},
                {text: 'Price', dataField: 'costBasisGLPerc'},
                {text: 'Avg. Invest. Per Share', dataField: 'hasOpen'},
                {text: 'Current Value', dataField: 'plans'},
                {text: 'Amount Invested', dataField: 'prdType'},
                {text: 'Invest. GL', dataField: 'quan'},
                {text: 'Est. Annual Income', dataField: 'shortName'}
            ]
        };
    }]);
})(angular, jqWidgetcontrollersModule);