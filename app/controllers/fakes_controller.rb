class FakesController < ApplicationController

  def holdings_print
    respond_to do |format|
      format.json {render json: 20000.times.map {|i| get_position("-#{i+1}")}}
    end
  end

  def holdings_print_schema
    respond_to do |format|
      format.json {render json: {data: 20000.times.map {|i| get_position("-#{i+1}")}}}
    end
  end

  private

  def get_position(postfix = '')
    {
        acctName: 'James, TA Trust Capital Access' + postfix,
        acctNum: '50450043',
        amountInv: nil,
        avgCostShare: nil,
        avgInvShare: nil,
        costBasis: nil,
        costBasisGL: nil,
        costBasisGLPerc: nil,
        current: 29426243.59,
        cusip: nil,
        daily: nil,
        dailyPerc: nil,
        desc: '2514 DIVERSIFIED L.P.',
        disp: '',
        estAnnInc: nil,
        estAnnYield: nil,
        hasOpen: false,
        hasTaxlots: false,
        id: '50450043_Cash',
        amountInvGL: nil,
        amountInvGLPerc: nil,
        isMarginTypeLoc: false,
        isTrust: false,
        mAgreements: false,
        percPort: 0.0398826162095903,
        plans: nil,
        pType: nil,
        sourceAcct: nil,
        price: nil,
        priceChg: nil,
        priceChgPerc: nil,
        priceDate: nil,
        prdType: 0,
        detailedPrdType: 0,
        quan: nil,
        rschRating: nil,
        shortName: 'JAMES,TA CAP AC',
        suitability: 0,
        symCusip: 'CASH',
        typeLocs: nil,
        isSweepOption: true,
        underlying: nil,
        isMoney: true,
        dSym: 'CASH',
        coupon: nil,
        debt: ' ',
        factor: nil,
        fCallDate: nil,
        fCallPrice: nil,
        hasPrerefund: false,
        maturity: nil,
        modDuration: nil,
        nextPayAmount: nil,
        nextPayDate: nil,
        par: nil,
        ratings: nil,
        remainPrincVal: nil,
        sCallDate: nil,
        sCallPrice: nil,
        cashBalances: [
            {
                sortOrder: 3,
                description: 'EAGLE CLASS - JPMORGAN TAX FREE MONEY MARKET FUND*',
                amount: 29426243.59,
                estAnnInc: 2942.62,
                estAnnYield: 0.009999985186692326
          }
        ],
        yestValue: nil,
        isMCB: false,
        isMAI: false,
        isCash: false,
        currency: ' ',
        exRate: nil,
        nonUsPrice: nil,
        nonUsValue: nil,
        cultureCode: nil,
        advisor: '0002',
        status: 0,
        ar: 'tsg',
        ac: 'tst',
        stc: nil,
        mm5: nil,
        pc: nil,
        pi: 'cash',
        psc: 'brk',
        marginable: false,
        policy: nil,
        grpSrt: nil,
        multiTradable: false,
        tradeMenuType: 7,
        selected: false
    }
  end
end
