class CitiesController < ApplicationController
  def sample
    @city = City.random

    respond_to do |format|
      format.json {render json: @city.in_json}
    end
  end

  def index
    @cities = City.all

    respond_to do |format|
      format.json {render json: @cities.map(&:in_json)}
    end
  end

  def jq_batch
    page = params[:page].to_i
    limit = params[:rows].to_i
    sidx = params[:sidx]
    sord = params[:sord]

    sidx = 1 if sidx.nil?
    limit = params[:totalrows].to_i unless params[:totalrows].nil?

    count = City.count
    total_pages = if count > 0
      (count.to_f / limit.to_f).ceil
    else
      0
    end

    page = total_pages if page > total_pages
    limit = 0 if limit < 0
    start = limit * page - limit
    start = 0 if start < 0

    # code here
    cities = City.order('?', sidx).limit(limit).offset(start).map {|c| {id: c.id, cell: [c.id, c.name, c.zip, c.state, c.country, c.time_zone]}}

    response = {
        page: page,
        total: total_pages,
        records: count,
        rows: cities
    }

    respond_to do |format|
      format.json {render json: response}
    end
  end

  def ng_batch
    respond_to do |format|
      format.json {render json: City.all.map {|c| {id: c.id, name: c.name, zip: c.zip, state: c.state, country: c.country, time_zone: c.time_zone}}}
    end
  end
end
