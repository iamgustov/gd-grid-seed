# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# City.delete_all
#
# 5000.times do |i|
#   city = City.create(name: Faker::Address.city, zip: Faker::Address.zip_code,
#               time_zone: Faker::Address.time_zone, state: Faker::Address.state,
#               country: Faker::Address.country)
#   puts "#{city.name} created..."
# end